#!/usr/bin/env python3

"""

    Script for slack management
    by Dorance <dorancemc@gmail.com>
    22-Nov-2023

"""

from slack_sdk import WebClient
import argparse
import libslack

parser = argparse.ArgumentParser()
parser.add_argument('--slack_token', required=True, help='Slack token')
parser.add_argument('--channel', required=True, help='Channel name')
parser.add_argument('--email', required=True, help='User email(s) separated by comma')
args = parser.parse_args()

client = WebClient(token=args.slack_token)

def main():
  emails = args.email.split(',')
  
  channel_id = libslack.get_channelid(client, args.channel)
  users_id = libslack.get_usersid(client, emails)

  if users_id and channel_id:
    resultado = libslack.add_userid2channelid(client, users_id, channel_id)
    if resultado:
      print(f"Usuario(s) agregado al canal con éxito.")
    else:
      print("Error al agregar usuario(s) al canal.")
  else:
    print("No se agregaron usuarios.")

if __name__ == "__main__":
  main()
