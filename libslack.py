#!/usr/bin/env python3

"""

    Script for slack management
    by Dorance <dorancemc@gmail.com>
    22-Nov-2023

"""

from slack_sdk.errors import SlackApiError

def get_userid(client, email):
    try:
        result = client.users_list()

        for usuario in result['members']:
            if usuario['profile'].get('email') == email:
                return usuario['id']
        return None
    except SlackApiError as e:
        print(f"Error al obtener la lista de usuarios: {e.response['error']}")
        return None

def get_usersid(client, emails):
    users_id = []

    try:
        result = client.users_list()

        for usuario in result['members']:
            if usuario['profile'].get('email') in emails:
              users_id.append(usuario['id'])
        return users_id
    except SlackApiError as e:
        print(f"Error al obtener la lista de usuarios: {e.response['error']}")
        return None

def get_channelid(client, nombre_canal):
    try:
        result = client.conversations_list(types="public_channel,private_channel")
        for canal in result['channels']:
            if canal['name'] == nombre_canal:
                return canal['id']
        print(f"No se encontró un canal con el nombre {nombre_canal}")
        return None
    except SlackApiError as e:
        print(f"Error al obtener la lista de canales: {e.response['error']}")
        return None

def add_userid2channelid(client, users_id, channel_id):
  try:
    result = client.conversations_invite(channel=channel_id, users=','.join(users_id))
    return result
  except SlackApiError as e:
    print(f"Error al agregar usuario al canal: {e.response['error']}")
